#!/usr/bin/env bash

set -euo pipefail

TOOL_NAME="dsearch"
TOOL_TEST="dsearch"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

list_all_versions() {
  echo "1.0
1.1
1.2
1.3
1.4
1.5
1.5.1
1.5.2
1.5.3"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    mkdir -p "$install_path/bin"
  
    touch "$install_path/bin/dsearch-init.R"
    echo "packages <- c('RCurl', 'rvest', 'stringr')" >> $install_path/bin/dsearch-init.R
    if [ "$HOME" == "/root" ]; then
      lib=`compgen -G "/etc/skel/R/x86_64-pc-linux-gnu-library"`
    else
      lib=`compgen -G "$HOME/R/x86_64-pc-linux-gnu-library"`
    fi
    if [ ! "$lib" = "" ]; then
      echo "Packages installed to: $lib"
      echo "install.packages('https://l_m.gitlab.io/r-webtools-package/webtools_0.7.16.tar.gz', lib='$lib')" >> $install_path/bin/dsearch-init.R
      echo "install.packages(setdiff( packages, rownames(installed.packages())), lib='$lib')" >> $install_path/bin/dsearch-init.R
      Rscript $install_path/bin/dsearch-init.R
    else
      echo "install.packages('https://l_m.gitlab.io/r-webtools-package/webtools_0.7.16.tar.gz')" >> $install_path/bin/dsearch-init.R
      echo "install.packages( setdiff( packages, rownames( installed.packages() ) ) )" >> $install_path/bin/dsearch-init.R
      Rscript $install_path/bin/dsearch-init.R
    fi
    
    touch "$install_path/bin/dsearch.R"
    echo "#!/usr/bin/R" >> $install_path/bin/dsearch.R
    echo "library(webtools)" >> $install_path/bin/dsearch.R
    echo "args <- commandArgs(trailingOnly = TRUE)" >> $install_path/bin/dsearch.R
    echo "eval(parse(text=args[1]))" >> $install_path/bin/dsearch.R
    chmod a+x "$install_path/bin/dsearch.R"

    touch "$install_path/bin/dsearch"
    chmod a+x "$install_path/bin/dsearch"
    cat ${plugin_dir}/lib/dsearch.txt > "$install_path/bin/dsearch"
    sed -i -r "s|^DIR=.*|DIR=\"$install_path/bin\"|g" "$install_path/bin/dsearch"
    
    if [ ! -d "$HOME/.cache/datlinux" ]; then
        mkdir -p "$HOME/.cache/datlinux"
    fi

    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
